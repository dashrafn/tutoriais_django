from rest_framework import permissions

class IsAuthorOrReadOnly(permissions.BasePermission):
   
    def has_object_permission(self, request, view, obj):
        # Permissões somente leitura são permitidas
        # para qualquer requisição
        if request.method in permissions.SAFE_METHODS:
            return True

        # As permissões de escrita são permitidas apenas
        # ao autor de uma postagem
        return obj.author == request.user